/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110waj
 *
 */
public class Celsius extends Temperature{
	public Celsius(float t){
		super(t);
	}
	public String toString(){
		//TODO: Complete this method
		String myString = "" + this.getValue() + " C";
		return myString;
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stub
		Celsius myCelsius = new Celsius(this.getValue());
		return myCelsius;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Fahrenheit myFahrenheit = new Fahrenheit(this.getValue() * 1.8f + 32);
		return myFahrenheit;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Kelvin myKelvin = new Kelvin(this.getValue() + 273.15f);
		return myKelvin;
	}
}
