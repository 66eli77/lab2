/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * TODO (cs110waj): write class javadoc
 *
 * @author cs110waj
 *
 */
public class Kelvin extends Temperature{
	public Kelvin(float t){
		super(t);
	}
	public String toString(){
		//TODO: Complete this method
		String myString = "" + this.getValue() + " K";
		return myString;
	}
	@Override
	public Temperature toCelsius(){
		//TODO: Complete this method
		Celsius myCelsius = new Celsius(this.getValue() - 273.15f);
		return myCelsius;
	}
	@Override
	public Temperature toFahrenheit(){
		//TODO: Complete this method
		Fahrenheit myFahrenheit = new Fahrenheit((this.getValue()-273.15f)* 1.8f + 32);
		return myFahrenheit;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Kelvin myKelvin = new Kelvin(this.getValue());
		return myKelvin;
	}
}
