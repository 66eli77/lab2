/**
 * 
 */
package edu.ucsd.cs110w.temperature;

/**
 * @author cs110waj
 *
 */
public class Fahrenheit extends Temperature{
	public Fahrenheit(float t){
		super(t);
	}
	public String toString(){
		//TODO: Complete this method
		String myString = "" + this.getValue() + " F";
		return myString;
	}
	@Override
	public Temperature toCelsius() {
		// TODO Auto-generated method stubs
		Celsius myCelsius = new Celsius((this.getValue() - 32) / 1.8f);
		return myCelsius;
	}
	@Override
	public Temperature toFahrenheit() {
		// TODO Auto-generated method stub
		Fahrenheit myFahrenheit = new Fahrenheit(this.getValue());
		return myFahrenheit;
	}
	@Override
	public Temperature toKelvin() {
		// TODO Auto-generated method stub
		Kelvin myKelvin = new Kelvin((this.getValue() - 32)/1.8f + 273.15f);
		return myKelvin;
	}
}
